let token = localStorage.getItem("token");

let profileContainer = document.querySelector('#profileContainer');

if(!token || token === null){

	alert('You must login first');
	window.location.replace("./login.html");

} else {

	fetch('https://peaceful-bastion-14168.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => { 
		console.log(data);
 
		profileContainer.innerHTML =  
		`
			<div class="col-md-12">
			<section class="jumbotron my-5">	

				<h3 class="text-center">First Name: ${data.firstName}</h3>
				<h3 class="text-center">Last Name: ${data.lastName}</h3>
				<h3 class="text-center">Email: ${data.email}</h3>
				<h3 class="text-center">Contact Number: ${data.mobileNo}</h3>
				<h3 class="mt-5">Class History</h3>

					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
						</thead>
						<tbody id="coursesContainer">
						</tbody>
					</table> 

				</section>
			</div>
		`

		let coursesContainer = document.querySelector('#coursesContainer');
		console.log(coursesContainer);


		data.enrollments.map(enrollment => {
			console.log(enrollment);

			fetch(`https://peaceful-bastion-14168.herokuapp.com/api/courses/${enrollment.courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				coursesContainer.innerHTML += 
				`
					<tr>
						<td>${data.name}</td>
						<td>${enrollment.enrolledOn}</td>
						<td>${enrollment.status}</td>
					</tr>
				`

			})

		})

	})

}








// let token = localStorage.getItem("token");

// let profileContainer = document.querySelector('#profileContainer');


// 	fetch('http://localhost:4000/api/users/details', {
// 		method: 'GET',
// 	  	headers: {
// 	    'Authorization': `Bearer ${userToken}`,
// 	  },
// 	})
// 	.then(res => res.json())
// 	.then(data => {
// 		console.log(data);

// 	    profileContainer.innerHTML = 
// 	    	`
// 	    	<main class="container my-5">
// 			    <div id="profileContainer" class="row">
// 			      <div class="col-md-12">
// 			        <section class="jumbotron text-center my-5">
// 			        <div>
// 			            <div>
// 			                <h5 id="fName" class="">First Name: </h5>
// 			                <h5 id="lName" class="">Last Name: </h5>
// 			                <h5 id="email" class="">Email: </h5>
// 			            </div><br>
// 			                <h3>Class History</h3>
// 			                <table class="table">
// 			                    <thead>
// 			                      <tr>
// 			                        <th scope="col">Course ID</th>
// 			                        <th scope="col">Enrolled On</th>
// 			                        <th scope="col">Status</th>
// 			                      </tr>
// 			                    </thead>
// 			                    <tbody id="coursesEnrolled">
// 			                  	  	<td id="courseId"></td>
// 									<td id="date"></td>
// 									<td id="status"></td>
// 			                    </tbody>
// 			                  </table>
// 			       		 </div>	
// 			     	 </div>
// 			        </section>
// 			      </div>
// 			    </div>
// 	    	`;

// 			let firstName = document.querySelector('#fName');
// 		    let lastName = document.querySelector('#lName');
// 		    let email = document.querySelector('#email');
		    
// 		    let courseId = document.querySelector('#courseId');
// 		    let date = document.querySelector('#date');
// 		    let status = document.querySelector('#status');

// 		    firstName.innerHTML = `<h5 id="fName" class="">First Name:${data.firstName}</h5>`;
// 		    lastName.innerHTML = `<h5 id="lName" class="">Last Name:${data.lastName}</h5>`;
// 		    email.innerHTML = `<h5 id="email" class="">Email:${data.email}</h5>`;

// 		    /*if (data.isAdmin == true)
// 		    	let	courses = data.enrollments.map(course => {
// 				console.log(course);
// 		    	fetch(`http://localhost:4000/api/courses/${course.courseId}`)
// 				.then(res => res.json())
// 				.then(data => {
// 					console.log(data);
// 			})
// */


//     			console.log(data.enrollments);
    
// 	  	// 	let courses = [];

// 			let	courses = data.enrollments.map(course => {
// 				console.log(course);
// 					fetch(`http://localhost:4000/api/courses/${course.courseId}`)
// 					.then(res => res.json())
// 					.then(data => {
// 						console.log(data.name);

// 						/*return 
// 			   				(
// 					      	`
// 					      	<tr>
// 					      		<td id="courseId">${course._Id}</td>
// 					        	<td id="date">${course.enrolledOn}</td>
// 					        	<td id="status">${course.status}</td>
// 					        </tr>	
// 					        `)
// 				        		profileContainer.innerHTML = courses;
		        		
// 		        			// console.log(courses);*/


// 					})		

// 		 })
    					
//   });





// <td id="courseId"></td>
// <td id="date"></td>
// <td id="status"></td>
// ${courses}
	
		 



		
			
				
	